#!/bin/bash
# Dependencies: spotify, yad, wmctrl
# XFCE users! Window Manager Tweaks > Skip windows that have "skip pager" or "skip taskbar" properties set...

iconSize=22 # notification area maximum icon size (px)
spotifyToggle="wmctrl -x -r spotify.Spotify -b toggle,skip_taskbar,skip_pager" # spotify --toggle-window

#make notification icon for Spotify
yad \
--notification \
--listen \
--no-middle \
--text=Spotify \
--image=/usr/share/pixmaps/spotify-client.png \
--icon-size=$iconSize \
--command="$spotifyToggle" \
--menu="Toggle Spotify!$spotifyToggle|Exit Spotify!killall spotify & killall yad" \
& \
#launch Spotify
spotify
